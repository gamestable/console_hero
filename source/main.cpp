#include <iostream>
#include <Windows.h>
#include <algorithm>
#include <string>
#include "level.h"
#include "resources.h"

//80x25 Std Console Size

using namespace std;

HANDLE hConOut = GetStdHandle(STD_OUTPUT_HANDLE);

bool tick = false;
bool dtick = true;
bool ttick = false;

void SetGCursor(int x, int y)
{
    COORD c;
    c.X = x;
    c.Y = y;
    SetConsoleCursorPosition(hConOut,c);
}

void draw_level()
{
    SetGCursor(0,0);
    for(int y=0;y<24;y++)
    {
        for(int x=0;x<79;x++)
        {
            if (level_1[y][x]==1)
                cout << "#";
            else if (level_1[y][x]==2)
                cout << "!";
            else if (level_1[y][x]==3)
                cout << "_";
            else if (level_1[y][x]==4)
                cout << "#";
            else if (level_1[y][x]==5)
                if (ttick)
                    cout << "V";
                else
                    cout << "v";
            else if (level_1[y][x]==6)
                if (dtick)
                    cout << "W";
                else
                    cout << "w";
            else if (level_1[y][x]==7)
                if (dtick)
                    cout << "$";
                else
                    cout << "s";
            else if (level_1[y][x]==9)
                cout << (char)15;
            else
                cout << " ";
        }
        cout << endl;
    }
    tick=!tick;
    if (tick)
        dtick=!dtick;
    if (dtick)
        ttick=!ttick;
}

void build_level()
{
    //memcpy(&level_1[])
    //level_1_b = level_1;
}

void rebuild_level()
{

}

void draw_clear(char r = 0, char g = 0, char b = 0)
{
    SetGCursor(0,0);
    for(int y=0;y<26;y++)
    {
        for(int x=0;x<80;x++)
        {
            if (level_1[y][x]==1)
                cout << "*";
        }
        cout << endl;
    }
}

char GenerateRandomChar()
{
    char c = rand()%26;
    return c;
}

void draw_random()
{
    SetGCursor(0,0);
    for(int y=0;y<24;y++)
    {
        for(int x=0;x<79;x++)
        {
            char t = 0;
            while(t<20)
                t = GenerateRandomChar();
            cout << t;
        }
        cout << endl;
    }
}

int getKey()
{
    for(int i = 8; i <= 256; i++)
    {
        if(GetAsyncKeyState(i) & 0x7FFF)
        {
            if( ( i >= 37 && i <= 40 ) || i == 0x20 )
                return i;
        }
    }
    return -1;
}

class Player
{
    public:
        int x = 0;
        int y = 0;
        int px = x;
        int py = y;

        char health = 3;
        int coins = 0;

        char spawn = 0;

        char jump = 0;
        bool ground = false;

        Player()
        {
            this->x = level_1_ply[this->spawn][0];
            this->px = this->x;
            this->y = level_1_ply[this->spawn][1];
            this->py = this->y;
        }

        void respawn()
        {
            if (health==0)
                this->game_over();
            this->health -= 1;
            this->x = level_1_ply[this->spawn][0];
            this->px = this->x;
            this->y = level_1_ply[this->spawn][1];
            this->py = this->y;
            this->jump = 0;
            getKey();
            rebuild_level();
        }

        void explode()
        {
            SetGCursor(this->x,this->y);
            cout << "X\a";
            SetGCursor(this->x,this->y+1);
            cout << "*";
            ::Sleep(200);
            SetGCursor(this->x,this->y-1);
            cout << "*";
            SetGCursor(this->x-1,this->y-1);
            cout << "*";
            SetGCursor(this->x+1,this->y-1);
            cout << "*";
            ::Sleep(200);
            SetGCursor(this->x,this->y-2);
            cout << "*";
            ::Sleep(1600);
            respawn();
        }

        void game_over()
        {
            system("Color 47");
            cout << "\a";
            ::Sleep(200);
            cout << "\a";
            ::Sleep(200);
            cout << "\a\a";
            ::Sleep(200);
            for(int y=0;y<25;y++)
            {
                for(int x=0;x<80;x++)
                {
                    SetGCursor(x,y);
                    cout << "=";
                    ::Sleep(1);
                }
            }
            while(1)
            {
                SetGCursor(4,21);
                cout << " x Game Over! x ";
                SetGCursor(24,21);
                cout << "Coins: ";
                for(int y=0;y<this->coins;y++)
                {
                    SetGCursor(31+y,21);
                    cout << "$";
                }
                if (this->coins==0)
                    cout << "NONE";
                ::Sleep(500);
                SetGCursor(4,21);
                cout << " X Game Over! X ";
                SetGCursor(24,21);
                cout << "Coins: ";
                for(int y=0;y<this->coins;y++)
                {
                    SetGCursor(31+y,21);
                    cout << "s";
                }
                if (this->coins==0)
                    cout << "n00b";
                ::Sleep(500);
            }
        }

        void win()
        {
            system("Color 67");
            cout << "\a";
            ::Sleep(50);
            cout << "\a";
            ::Sleep(50);
            cout << "\a\a";
            ::Sleep(200);
            for(int y=0;y<25;y++)
            {
                for(int x=0;x<80;x++)
                {
                    SetGCursor(x,y);
                    cout << "#";
                    ::Sleep(1);
                }
            }
            while(1)
            {
                SetGCursor(4,21);
                cout << " $  You win!  $ ";
                SetGCursor(24,21);
                cout << "Coins: ";
                for(int y=0;y<this->coins;y++)
                {
                    SetGCursor(31+y,21);
                    cout << "$";
                }
                if (this->coins==0)
                    cout << "NONE";
                ::Sleep(500);
                SetGCursor(4,21);
                cout << "  $ You win! $  ";
                SetGCursor(24,21);
                cout << "Coins: ";
                for(int y=0;y<this->coins;y++)
                {
                    SetGCursor(31+y,21);
                    cout << "s";
                }
                if (this->coins==0)
                    cout << "n00b";
                ::Sleep(500);
            }
        }

        void die()
        {
            SetGCursor(this->x,this->y);
            cout << "X\a";
            ::Sleep(2000);
            respawn();
        }
        void fall()
        {
            SetGCursor(this->x,this->y);
            cout << ".\a";
            ::Sleep(2000);
            respawn();
        }
        void burn()
        {
            //SetGCursor(this->x,this->y);
            cout << "\a";
            ::Sleep(200);
            SetGCursor(this->x,this->y-1);
            cout << (char)178;
            ::Sleep(200);
            SetGCursor(this->x,this->y-2);
            cout << (char)177;
            ::Sleep(200);
            SetGCursor(this->x,this->y-3);
            cout << (char)176;
            ::Sleep(2000);
            respawn();
        }

        void pPos(int x, int y)
        {
            this->px = this->x;
            this->py = this->y;
            if (y>-1)
                this->y = y;
            if (x>-1)
                this->x = x;
        }

        bool go(int dir)
        {
            if (dir==VK_LEFT)
            {
                if (level_1[this->y][this->x-1]!=1)
                {
                    pPos(this->x-1,-1);
                    return true;
                }
                else if (level_1[this->y][this->x-1]==3)
                {
                    this->explode();
                    return true;
                }

            }
            else if (dir==VK_RIGHT)
            {
                if (level_1[this->y][this->x+1]!=1)
                {
                    pPos(this->x+1,-1);
                    return true;
                }
            }
            else if (dir==VK_SPACE)
            {
                if (this->ground)
                    this->jump = 5;
            }
        }

        void update()
        {
            go(getKey());
            if (this->y>=23)
            {
                fall();
            }
            if (level_1[this->y][this->x]==6)
            {
                burn();
            }
            if (level_1[this->y][this->x]==3)
            {
                explode();
            }
            if (jump==0)
            {
                if (level_1[this->y+1][this->x]==1)
                {
                    ground = true;
                }
                else
                {
                    pPos(-1,this->y+1);
                    ground = false;
                }
            }
            else
            {
                jump--;
                ground = false;
                if (level_1[this->y-1][this->x]==1)
                {
                    jump = 0;
                }
                else if (level_1[this->y-1][this->x]==8)
                {
                    level_1[this->y-1][this->x] = 1;
                    jump = 0;
                }
                else
                {
                    pPos(-1,this->y-1);
                }
            }
            if (level_1[this->y][this->x]==2)
            {
                level_1[this->y][this->x] = 0;
                this->spawn += 1;
            }
            if (level_1[this->y+1][this->x]==4)
            {
                level_1[this->y+1][this->x] = 0;
                ground = false;
            }
            if (level_1[this->y][this->x]==4)
            {
                level_1[this->y][this->x] = 0;
                ground = false;
            }
            if (level_1[this->y][this->x]==7)
            {
                level_1[this->y][this->x] = 0;
                this->coins += 1;
                cout << "\a";
            }
            if (level_1[this->y][this->x]==9)
            {
                win();
            }
        }

        void draw()
        {
            SetGCursor(this->x,this->y);
            cout << (char)12;
        }

        void drawHealth()
        {
            SetGCursor(0,0);
            for(int y=0;y<this->health;y++)
                cout << "+";
            cout << " ";
            SetGCursor(0,1);
            for(int y=0;y<this->coins;y++)
                cout << "$";
        }
};

void slowType(string in)
{
    for(int i=0;i<in.length();i++)
    {
        cout << in[i];
        ::Sleep(100);
    }
}

int main()
{
    //PlaySound("FIRE", GetModuleHandle(NULL), SND_RESOURCE);
    cout << title << endl <<endl;
    string in = "";
    while(1)
    {
        cout << "C:\\>";
        cin >> in;

        transform(in.begin(), in.end(), in.begin(), ::tolower);
        if (in=="start")
            break;
        else if (in=="help")
        {
            cout << endl << "START      Starts the Game";
            cout << endl << "HELP       Display this Text";
            cout << endl << "INFO       Display game Info";
            cout << endl << "FIX        Fix the Color Console Bug";
            cout << endl << "END        Close the Game" << endl << endl;
        }
        else if (in=="end")
            exit(0);
        else if (in=="fix")
        {
            system("Color 07");
            cout << "Color is resetted. Close the game? (y): ";
            char in = 0;
            cin >> in;
            if (in=='y'||in=='Y')
                exit(0);
            else
                cout << endl;
        }
        else if (in=="info")
        {
            tutorial[139] = 15;
            cout << endl << "Console_Hero v1.1" << endl << "Created by Steffen K. 10.11.2015" << endl << "Music: 404error - x-Ray/Greg http://grayscale.scene.pl/msx_archive.php" << endl << endl << tutorial << endl << endl;
        }
        else
            cout << "'" << in << "'is not recognized as an internal or external command," << endl << "operable program or batch file." << endl << endl;
    }
    cout << "C:\\>";
    slowType("format C:\\ /x /q");
    PlaySound("ERROR", GetModuleHandle(NULL), SND_RESOURCE | SND_ASYNC | SND_LOOP);
    cout << endl;
    ::Sleep(100);
    cout << "   C:\\Users" << endl;
    ::Sleep(600);
    cout << "   C:\\WINDOWS" << endl;
    ::Sleep(600);
    cout << "   C:\\Program Files" << endl;
    ::Sleep(200);
    //::Sleep(300);
    for(int x=0;x<7;x++)
    {
        draw_random();
        ::Sleep(200);
    }
    ::Sleep(200);

    draw_clear();
    system("Color 3A");

    Player ply;
    bool run = true;
    while(run)
    {
        draw_level();
        ply.update();
        ply.draw();
        ply.drawHealth();
        ::Sleep(45);
    }
    PlaySound(NULL, 0, SND_ASYNC);
    return 0;
}
